// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBpqJhg58b2LtpMBt3F1cQZVKcBvUVMT7w',
    authDomain: 'login-task-13c36.firebaseapp.com',
    databaseURL: 'https://login-task-13c36.firebaseio.com',
    projectId: 'login-task-13c36',
    storageBucket: 'login-task-13c36.appspot.com',
    messagingSenderId: '879671033562',
    appId: '1:879671033562:web:8ee1ae41aa29fc446dafe3',
    measurementId: 'G-0Z6QXJJFLW'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
