import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage: string = null;

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {
  }

  ngOnInit(): void {
    // other way  i can use formBuilder Validation
    // email: ['',Validators.required]
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });

  }

  login() {

    this.authService.logIn(this.loginForm.value.email, this.loginForm.value.password).then(result => {
      this.authService.getUserData(result.user.uid);

    }).catch((error) => {
      this.errorMessage = error.message;
    });

  }
}
