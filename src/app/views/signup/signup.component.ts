import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  countries: any = ['jordan', 'usa'];

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      email: [''],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      hobby: this.formBuilder.array([]),
      age: [''],
      country: ['jordan']
    }, {
      validator: this.mustMatch('password', 'confirmPassword')
    });

  }

  get f() {
    return this.signupForm.controls;
  }

  initHobby(): FormGroup {
    return this.formBuilder.group({
      value: ['', Validators.required],
    });
  }

  addNewHobby() {
    const add = this.signupForm.get('hobby') as FormArray;
    add.push(this.initHobby());
  }


  deleteHobby(index: number) {
    const add = this.signupForm.get('hobby') as FormArray;
    add.removeAt(index);
  }

  signup() {
    if (this.signupForm.invalid) {
      return;
    }
    this.authService.signUp(this.signupForm.value);
  }


  mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

}
