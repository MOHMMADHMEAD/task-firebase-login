import {Component} from '@angular/core';
import {AuthService} from './shared/services/auth.service';
import {User} from './shared/interfaces/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'firebase-login';
  user: User;

  constructor(public authService: AuthService) {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
  }

  logout() {
    this.authService.logOut();
  }
}
