export interface User {
  uid: string;
  email: string;
  age: number;
  password: string;
  country: string;
  hobby: any;
}
