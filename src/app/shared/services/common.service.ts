import {Injectable} from '@angular/core';
import {NgxNotificationService} from 'ngx-notification';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private ngxNotificationService: NgxNotificationService) {
  }


  handlerNotification(type: string, message: string) {
    switch (type) {
      case 'error':
        this.ngxNotificationService.sendMessage(message, 'danger', 'top-right');
        break;
      case 'success':
        this.ngxNotificationService.sendMessage(message, 'success', 'top-right');
        break;
    }

  }
}
