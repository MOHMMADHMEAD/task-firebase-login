import {Injectable, NgZone} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {User} from '../interfaces/user';
import {Subject} from 'rxjs';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: User;

  userLogin = new Subject();

  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router,
    private commonService: CommonService
  ) {
  }

  logIn(email, password) {
    return this.afAuth.signInWithEmailAndPassword(email, password);

  }

  signUp(data) {
    return this.afAuth.createUserWithEmailAndPassword(data.email, data.password)
      .then(async (result) => {
        console.log(result);
        await this.setUserData(result.user, data);
        this.commonService.handlerNotification('success', 'Success Signup');
        this.router.navigateByUrl('/login');
      }).catch((error) => {
        this.commonService.handlerNotification('error', error.message);
      });
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }


  setUserData(user, data) {
    const userData: User = {
      uid: user.uid,
      email: user.email,
      password: data.password,
      age: data.age,
      hobby: data.hobby,
      country: data.country
    };
    return this.afs.collection(`users`).add(userData);


  }

  getUserData(userId) {
    let user = null;
    this.afs.collection('users', ref => ref.where('uid', '==', userId.toString())).snapshotChanges().subscribe((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        user = Object.assign({}, {}, doc.payload.doc.data());
      });
      localStorage.setItem('user', JSON.stringify(user));
      if (user) {
        this.router.navigateByUrl('/home');
      }
    });
  }

  logOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.clear();
      this.router.navigateByUrl('/login');
    });
  }
}
